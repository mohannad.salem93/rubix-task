import React from 'react';
import Todo from "./components/TodoList/Todo"
import './App.css';

function App() {
  return (
    <div className="App">
      <Todo />
    </div>
  );
}

export default App;
