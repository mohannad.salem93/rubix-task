import React, { useContext } from "react"
import { TodoContext } from "./TodoContext"
import { v4 as uuidv4 } from 'uuid';
import { Button, Modal, Form } from 'semantic-ui-react'
// Support for date & time is still missing 
export default () => {
  const { list, setList, openModal, setModalVisibility, todo, setTodo } = useContext(TodoContext)
  return (<Modal size={"small"} open={openModal} onClose={() => setModalVisibility(false)}>
    <Modal.Header>{`${todo.id ? "Edit" : "New"} Task`}</Modal.Header>
    <Modal.Content>
      <Form>
        <Form.Group widths='equal'>

          <Form.Field>
            <label>Title</label>
            <input placeholder='task' value={todo.title} onChange={e => setTodo({ ...todo, title: e.target.value })}></input>
          </Form.Field>
          <Form.Field>
            <label>Due Date</label>
            <input type="datetime-local" value={todo.dueDate} onChange={e => setTodo({ ...todo, dueDate: e.target.value })}></input>
          </Form.Field>

        </Form.Group>

        <Form.Field>
          <label>Description</label>
          <textarea value={todo.description} onChange={e =>
            setTodo({ ...todo, description: e.target.value })
          } rows="2" cols="30" />
        </Form.Field>

      </Form>
    </Modal.Content>
    <Modal.Actions>
      <Button onClick={() => {
        setModalVisibility(false)
        setTodo({ id: "", title: "", description: "", dueDate: "" })
      }} content='Cancel' />
      <Button
        onClick={() => {
          const taskExists = list.find(rec => rec.id === todo.id)
          if (taskExists) {
            //Update Task
            setList(list.map(rec => rec.id === taskExists.id ? todo : rec))
          } else {
            //Create Task
            setList(list.concat({ ...todo, id: uuidv4() }))
          }
          setModalVisibility(false)
          setTodo({ id: "", title: "", description: "", dueDate: "" })
        }}
        primary
        content='Submit'
      />

    </Modal.Actions>
  </Modal >);
}