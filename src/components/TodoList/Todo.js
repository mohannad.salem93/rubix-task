import React, { useState } from "react"
import { Header, Segment, Grid, Button, List } from 'semantic-ui-react'
import { TodoContext } from "./TodoContext"
import Modal from "./Modal"
import "./Todo.css"
export default () => {

  const [openModal, setModalVisibility] = useState(false)
  const [list, setList] = useState([])
  const [todo, setTodo] = useState({ id: "", title: "", description: "", dueDate: "", completed: false })

  const provider = {
    openModal, setModalVisibility, list, setList, todo, setTodo
  }

  return <TodoContext.Provider value={provider}>
    <div className="todo-page">
      <Segment>
        <Header as='h1' textAlign='left'>
          <Grid >
            <Grid.Column floated='left' width={5}>
              TODO LIST
            </Grid.Column>
            <Grid.Column floated='right' width={1}>
              <Button
                size="small"
                onClick={() => setModalVisibility(true)}
                color="blue"
                content="ADD"
              />
            </Grid.Column>
          </Grid>
        </Header>
        <Header as="h3" textAlign="left">
          Tasks
        </Header>
        <List divided relaxed className="task-list">
          {list.filter(el => !el.completed).map(el => <List.Item key={el.id}>
            <List.Content floated='right'>
              <Button.Group size='mini'>
                <Button onClick={() => {
                  setModalVisibility(true)
                  setTodo({ ...el })
                }} content="Edit" icon="pencil" />
                <Button.Or />
                <Button onClick={() => setList(list.map(task => task.id === el.id ? { ...task, completed: true } : task))} content="Done" icon="check circle" color="green" />
              </Button.Group>
            </List.Content>
            <List.Icon name='tasks' />
            <List.Content>

              <List.Header as='a'>{el.title}</List.Header>
              <List.Description>
                {el.description}
              </List.Description>
            </List.Content>
          </List.Item>)}

        </List>
        <Header as="h3" textAlign="left">
          Completed Tasks
        </Header>
        <List divided relaxed className="task-list">
          {list.filter(el => el.completed).map(el => <List.Item key={el.id}>
            <List.Content floated='right'>
              <Button basic secondary size='mini' onClick={() => setList(list.map(task => task.id === el.id ? { ...task, completed: false } : task))} content="Revert" icon="undo" />
            </List.Content>
            <List.Icon name='tasks' />
            <List.Content>
              <List.Header className="task-completed" as='a'>{el.title}</List.Header>
            </List.Content>
          </List.Item>)}

        </List>

        <Modal />
      </Segment>
    </div>
  </TodoContext.Provider >

};